import React from 'react'
import Link from 'gatsby-link'
import {Navbar,Nav,Collapse,NavItem,NavbarToggler} from "reactstrap"
 import "bootstrap/dist/css/bootstrap.css"
 import "./style.css"
 import "@fortawesome/fontawesome-free";
 import "@fortawesome/fontawesome-free/css/all.css"
class Header extends React.Component
{
  constructor(props)
  {
    super(props);
    this.state = {
      isOpen:false
    }
  }
  toggle(){
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  addButton()
  {
    if(!this.state.isOpen)
     return <span className="navbar-toggler-icon"></span>

     return <i className="fas fa-times fa-2x"></i>
  }
  render()
  {
    return (<div>
      <Navbar id="g"  light expand="md">
      <a className="navbar-brand">Ahmed Khattab</a>
      <button className="navbar-toggler"  onClick={() => this.toggle()}>
         {this.addButton()}
      </button>
      <Collapse isOpen={this.state.isOpen} navbar>
        <Nav className="ml-auto" navbar>
         <NavItem>
          <Link to="/" className="nav-link">Home</Link>
         </NavItem>
         <NavItem>
         <Link to="/Works" className="nav-link">Works</Link>
        </NavItem>
         <NavItem>
          <Link to="/about" className="nav-link">About</Link>
         </NavItem>
         <NavItem>
          <Link to="/contact" className="nav-link">Contact</Link>
         </NavItem>
        </Nav>
       </Collapse>
      </Navbar>

      </div>)
  }
}
export default Header