import React from "react";
import Link from "gatsby-link";
import {Button,Row,Col,Container,Form,Input,CardHeader,CardBody,FormGroup,Card} from "reactstrap"
export default class contact extends React.Component{
    render(){
        return( <Container>
            <h1 className="text-center">Contact</h1>
               <Row>
                  <Col lg="12" sm="12">
                    <h2 className="text-center">Via Email</h2>
                     <Form>
                     <Input type="text" placeholder="Your Name" className="mt-2"/>
                     <Input type="text" placeholder="Your Email" className="mt-2" />
                     <textarea className="form-control mt-2" rows={8} placeholder="Your text"></textarea>
                      <Button type="button" className="mt-2 btn btn-info">Send</Button>
                     </Form>
                  </Col>
               </Row>
               <Row>
                  <Col md="12">
                  <h2 className="text-center">Social Media</h2>
                  </Col>
               </Row>
               <Container>
               <Row>
               <Col lg="4" md="3" sm="6">
                 <h3 className="text-center"><a className="link facebook" href="https://www.facebook.com/profile.php?id=100007577736261"><i data-text="FaceBook" className="fab fa-facebook fa-3x"></i></a></h3>
                 <h4 className="text-center">FaceBook</h4>
                 </Col>
               <Col lg="4" md="3" sm="6">
               <h3 className="text-center"><a className="link instagram" href="https://www.instagram.com/ahmad_xatab/"><i className="fab fa-instagram fa-3x"></i></a></h3>
               <h4 className="text-center">Instagram</h4>
             </Col>
             <Col lg="4" md="3" sm="6">
             <h3 className="text-center"><a className="link github" href="https://github.com/A7madXatab" ><i className="fab fa-github fa-3x"></i></a></h3>
             <h4 className="text-center">GitHub</h4>
           </Col>
           <Col lg="4" md="3" sm="6">
             <h3 className="text-center"><a className="link twitter" href="https://twitter.com/ahmadghost0_0?lang=en" ><i className="fab fa-twitter fa-3x"></i></a></h3>
             <h4 className="text-center">Twitter</h4>
           </Col>
           <Col lg="4" md="3" sm="6">
             <h3 className="text-center link"><i className="fas fa-mobile fa-3x"></i></h3>
             <h4 className="text-center">+964 750 362 0373</h4>
           </Col>
           <Col lg="4" md="3" sm="6">
             <h3 className="text-center link"><i className="fab fa-skype fa-3x"></i></h3>
             <h4 className="text-center">live:ahmadzendane725</h4>
           </Col>
            </Row>
               </Container>
               <Row>
               </Row>
               
            </Container>)
    }
}