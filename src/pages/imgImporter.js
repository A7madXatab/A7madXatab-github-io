import Home from "./Images/first-website/Home-1.png";
import Home2 from "./Images/first-website/Home-2.png"
import Home3 from "./Images/first-website/Home-3.png"
import Rooms from "./Images/first-website/Rooms-1.png"
import Rooms2 from "./Images/first-website/Rooms-2.png"
import Rooms3 from "./Images/first-website/Rooms-3.png"
import contact from "./Images/first-website/contact-1.png"
import contact1 from "./Images/first-website/contact-2.png";

import BlogHome from "./Images/Blog/Home-1.png";
import posts from "./Images/Blog/posts-1.png";
import posts2 from "./Images/Blog/posts-2.png";
import users from "./Images/Blog/Users.png"
import comments from "./Images/Blog/comments.png";
import categories from "./Images/Blog/Categories.png"

import toDo from "./Images/toDolist/toDo.png";
const firstProjectImages = [{projectName:"Sweet Dreams Hostel MockUp"},
    {src:Home},{src:Home2},{src:Home3},{src:Rooms},{src:Rooms2},{src:Rooms3},{src:contact,contact1},
{link:"https://github.com/A7madXatab/a7madxatab.github.io-website-mockup"}];
const blogImages=[{projectName:"Blog"},
    {src:BlogHome},{src:posts},{src:posts2},{src:users},{src:comments},{src:categories}
,{link:"https://github.com/A7madXatab/A7madXatab.github.io"}];
 
const toDoList = [{projectName:"ToDOList"},{src:toDo},{link:'https://github.com/A7madXatab/todolist-with-React.js'}];
let Obj = [
    blogImages,firstProjectImages,toDoList
  ];
export default Obj;