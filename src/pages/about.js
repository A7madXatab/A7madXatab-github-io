import React from "react";
import Link from 'gatsby-link'
import {Card,CardBody,CardHeader,Col,Container,Row,CardFooter} from "reactstrap"
import "./style.css";
import "@fortawesome/fontawesome-free";
import "@fortawesome/fontawesome-free/css/all.css"
import im from "./Images/Me.jpg"
export default class About extends React.Component
{
    render()
    {
        return (
         <div>
            <Container className="text-center">
              <Col className="text-center">
               <h1>About Me</h1>
               <hr></hr>
                <h3>Allow Me To Introduce MySelf</h3>
              </Col>
            </Container> 
            <Container>
              <Row>
              <Col lg="6" md="7" sm="12">
                 <img alt="not loading" src={im}></img>
               </Col>
               <Col lg="6" md="5" sm="12" className="text-left">
                 <p className="about">  Im ahmad khattab ahmad, 18, born and living in erbil,Kurdistan.
                 a student at software enginnering in salahhading University,Erbil.
                 a lover for programming and gaming, started programming at 17,
                 currently a second stage student in the software enginnering department at salahaddin Uni.
                 enthusiastic, eager to alwayas learn something new.Learned web development in <a><span>Re:Coded, </span></a> 
                  done a bunch of projects both in the field of front-end and creating management systems with JAVA, with these set of 
                 knowledge i gurantee to deliver high quality error-free website for you and your team. </p>
                 <h4>MY Intrests</h4>
                 <p>
                 Very intrested in gaming and programming, cars, technologies, etc.
                 also intrested in android programming and will learn it soon
                 </p>
               </Col>
              </Row>
              <Card className="mt-3 outline-primary">
              <CardHeader className="text-center about">What I Can Do For You</CardHeader>
              <CardBody>
              <Row>
                 <Col className="text-center" lg="6" md="6" sm="12">
                   <h2 className="html"><i className="html fab fa-html5 fa-2x"></i> <br/> <hr className="mt-3"/></h2>
                   <p className="text-left">Working with the latest version of HTML, HTML 5</p>
                 </Col>
                 <Col className="text-center" lg="6" md="6" sm="12">
                 <h4><i className="fab fa-css3 fa-3x css"></i><br/><hr className="mt-3"/></h4>
                 <p className="text-left">Working with the latest version of CSS, CSS 3 <br />
                 as well as the latest version of <Link to="/" className="bootsrap"><span data-text="Bootstrap">Bootstrap</span></Link></p>
               </Col>
              </Row>
              <Row>
              <Col className="text-center" lg="6" md="6" sm="12">
              <h4><i className="fab fa-js fa-3x js"></i> <br/><hr className="mt-3"/></h4>
              <p className="text-left">Comfortable with JavaScript and some of the JavaScript libraries such as <a href="https://reactjs.org/" className="library"><span data-text="React">React</span></a>
               and <a className="library" href="https://jquery.com/"><span data-text="JQuery">JQuery</span></a>
              </p>
            </Col>
              <Col className="text-center" lg="6" md="6" sm="12">
              <h4><i className="fab fa-java fa-3x java"></i><br/><hr className="mt-3"/></h4>
              <p className="text-left">Beside from the Web side, also i can comfortably use java, and a good understanding of the java language
              OOP, Generics, Data Structure, Lambda Expressions.
              </p>
            </Col>
              </Row>
              </CardBody>
              <CardFooter className="about d-flex justify-content-between">
               <Link to="/Works" className="WorksLink">Take A Look At My Work</Link>
               <small>Icons Provided By <a className="WorksLink" href="https://fontawesome.com/">Fontawsome</a></small>
              </CardFooter>
               </Card>
            </Container>
        </div>
        )
    }
}