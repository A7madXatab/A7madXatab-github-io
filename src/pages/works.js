import React, { Component } from 'react';
import Link from "gatsby-link";
import {Col,Row,Container,
  Carousel,
  CarouselItem,
  CarouselControl,
  CarouselIndicators,
  CardHeader,
  Card,
  CardFooter,
  Jumbotron
} from 'reactstrap';
import Obj from "./imgImporter";
class RenderCarousel extends React.Component{
  constructor(props){
    super(props);
  }
  render()
  {
    return (
      <Container>
        <h1 className="text-center">My Previous Works</h1>
         <hr></hr>
        {Obj.map(arr => {
          let projectName = arr[0].projectName;
          let projectLink = arr[arr.length-1].link;
          arr = arr.slice(1,arr.length-1);
          <h1>projectName</h1>
          return <Example link={projectLink} name={projectName} arr={arr}></Example>
        })}
        <Col className="text-center More mt-3">
          <h4>Want to Look at More ?</h4>
          <a className="btn  btn-info" href="https://github.com/A7madXatab?tab=repositories">Sure !</a>
        </Col>
        
      </Container>
    )
 }
}
class Example extends Component {
  constructor(props) {
    super(props);
    this.state = { activeIndex: 0 };
    this.next = this.next.bind(this);
    this.previous = this.previous.bind(this);
    this.goToIndex = this.goToIndex.bind(this);
    this.onExiting = this.onExiting.bind(this);
    this.onExited = this.onExited.bind(this);
    this.arr = this.props.arr;
    this.name = this.props.name;
  }

  onExiting() {
    this.animating = true;
  }

  onExited() {
    this.animating = false;
  }

  next() {
    if (this.animating) return;
    const nextIndex = this.state.activeIndex === this.arr.length - 1 ? 0 : this.state.activeIndex + 1;
    this.setState({ activeIndex: nextIndex });
  }

  previous(arr) {
    if (this.animating) return;
    const nextIndex = this.state.activeIndex === 0 ? this.arr.length - 1 : this.state.activeIndex - 1;
    this.setState({ activeIndex: nextIndex });
  }

  goToIndex(newIndex) {
    if (this.animating) return;
    this.setState({ activeIndex: newIndex });
  }

  render() {
    const { activeIndex } = this.state;
    const slides = this.arr.map((item) => {
      return (
        <CarouselItem
          onExiting={this.onExiting}
          onExited={this.onExited}
        >
        <img src={item.src} />
        </CarouselItem>
      );
    });
    return (
           <Row className={this.props.name}>
            <Card className="mt-4">
            <CardHeader className="project-name">{this.props.name}</CardHeader>
            <Carousel
            activeIndex={activeIndex}
            next={this.next}
            previous={this.previous}
          >
            <CarouselIndicators className="g" items={this.arr} activeIndex={activeIndex} onClickHandler={this.goToIndex} />
            {slides}
            <CarouselControl direction="prev" directionText="Previous" onClickHandler={ this.previous} />
            <CarouselControl direction="next" directionText="Next" onClickHandler={this.next} />
          </Carousel>
          <CardFooter className="project-link"><a className="btn btn-outline-secondary" href={this.props.link}>Project Link</a></CardFooter>
            </Card>
          </Row>
        )
  }
}

export default RenderCarousel;